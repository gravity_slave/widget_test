import { CryptiumPage } from './app.po';

describe('cryptium App', () => {
  let page: CryptiumPage;

  beforeEach(() => {
    page = new CryptiumPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
