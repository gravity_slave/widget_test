import { Component, OnInit } from '@angular/core';
import { AppService } from './app.service';
import { attribute, MarketInterface, SortInterface } from './app.interfaces';
import { IntervalObservable } from 'rxjs/observable/IntervalObservable';
import 'rxjs/add/operator/takeWhile';
import * as _ from 'lodash';
import { Observable } from 'rxjs/Observable';
import { AppCacheService } from './app-cache.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent implements OnInit {
  marketInterface: MarketInterface = {
    rowsToDisplay: 'All',
    rightColumn: 'Name',
    quickSelect: 'None',
    filterWord: ''
  };
  sortingInterface: SortInterface = {
    sortingAttr: '',
    sortingOrder: 'asc'
  };
  currencies: Object[];
  displayOptions = false;
  displayOnlyFavorite = false;
  constructor(
    private cryptoService: AppService,
    private cacheService: AppCacheService
  ) {
    IntervalObservable.create(60000).subscribe(() => {
      this.cryptoService.getCurrency().subscribe(this.subscribeMe.bind(this));
    });
  }

  ngOnInit() {
    const subscribtion = this.cryptoService.getCurrency().subscribe(
      val => {
        this.subscribeMe(val);
      },
      err => console.error(err.message),
      () => subscribtion.unsubscribe()
    );
  }

  subscribeMe(val: Observable<Object>) {
    let dupCurrencies, currencyToCache, subscribtion;
    if (this.currencies) {
      dupCurrencies = [...this.currencies];
    }
    currencyToCache = val;
    for (let type in currencyToCache) {
      if (type.startsWith('USDT')) {
        currencyToCache[type]['isFavorite'] = dupCurrencies
          ? _.find(
              dupCurrencies,
              coin => coin.id === currencyToCache[type]['id']
            )['isFavorite']
          : false;
        currencyToCache[type]['coin'] = type.replace('USDT_', '');
      } else {
        continue;
      }
    }
    this.cacheService.setCoins(currencyToCache);
    subscribtion = this.cacheService.getCoins().subscribe(coins => {
      this.currencies = coins;
    });
    subscribtion.unsubscribe();
  }

  sortCrypt(attr: attribute) {
    if (this.sortingInterface.sortingAttr === '') {
      this.sortingInterface.sortingAttr = attr;
    } else {
      this.sortingInterface.sortingAttr = attr;
      this.sortingInterface.sortingOrder =
        this.sortingInterface.sortingOrder === 'asc' ? 'desc' : 'asc';
    }
  }

  sortCurrent(crypts: Object[]) {
    switch (this.sortingInterface.sortingAttr) {
      case 'icon':
        if (this.sortingInterface.sortingOrder === 'asc') {
          return _.sortBy(crypts, cr => cr['isFavorite']);
        } else {
          return _.sortBy(crypts, cr => !cr['isFavorite']);
        }
      case 'coin':
        if (this.sortingInterface.sortingOrder === 'asc') {
          return _.orderBy(crypts, ['coin'], ['asc']);
        } else {
          return _.orderBy(crypts, ['coin'], ['desc']);
        }

      case 'price':
        if (this.sortingInterface.sortingOrder === 'asc') {
          return _.orderBy(crypts, ['last'], ['asc']);
        } else {
          return _.orderBy(crypts, ['coin'], ['desc']);
        }
      case 'volume':
        if (this.sortingInterface.sortingOrder === 'asc') {
          return _.orderBy(crypts, ['quoteVolume'], ['asc']);
        } else {
          return _.orderBy(crypts, ['quoteVolume'], ['desc']);
        }
      case 'change':
        if (this.sortingInterface.sortingOrder === 'asc') {
          return _.orderBy(crypts, ['percentChange'], ['asc']);
        } else {
          return _.orderBy(crypts, ['percentChange'], ['desc']);
        }
      default:
        return crypts;
    }
  }

  get getCurrencies(): Array<Object> {
    let retCurrencies = [];
    if (this.marketInterface.rowsToDisplay === 'All') {
      retCurrencies = this.currencies;
      if (this.displayOnlyFavorite) {
        retCurrencies = this.currencies.filter(cur => cur['isFavorite']);
      }
    } else if (typeof this.marketInterface.rowsToDisplay === 'number') {
      if (this.displayOnlyFavorite) {
        retCurrencies = _.slice(
          this.currencies.filter(cur => cur['isFavorite']),
          0,
          this.marketInterface.rowsToDisplay
        );
      }
      retCurrencies = _.slice(
        this.currencies,
        0,
        this.marketInterface.rowsToDisplay
      );
    }

    if (this.marketInterface.filterWord) {
      retCurrencies = retCurrencies.filter(coin =>
        coin['coin'].toLowerCase().includes(this.marketInterface.filterWord)
      );
    }
    if (this.sortingInterface.sortingAttr) {
      retCurrencies = this.sortCurrent(retCurrencies);
    }
    return retCurrencies;
  }

  setFavorite(obj: { isFavorite: boolean }) {
    obj.isFavorite = !obj.isFavorite;
  }

  isNegative(strNum: string): boolean {
    return Math.sign(parseInt(strNum, 10)) === -1;
  }
}
