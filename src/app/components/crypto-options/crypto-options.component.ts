import { Component, OnInit, Input, forwardRef } from '@angular/core';
import { MarketInterface } from '../../app.interfaces';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'app-crypto-options',
  templateUrl: './crypto-options.component.html',
  styleUrls: ['./crypto-options.component.sass'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => CryptoOptionsComponent),
      multi: true
    }
  ]
})
export class CryptoOptionsComponent implements OnInit, ControlValueAccessor {
  readonly rowsToDisplay = [5, 10, 20, 30, 'All'];
  readonly rightColumn = ['Name', 'Balance', 'Est. Value'];
  readonly quickSelect = ['All', 'Balance', 'None'];
  marketInterface: MarketInterface;
  onChange = () => {};
  onTouched = () => {};

  constructor() {}

  ngOnInit() {}

  writeValue(marketInterface: MarketInterface): void {
    if (!marketInterface) {
      marketInterface = {
        rightColumn: '',
        quickSelect: '',
        rowsToDisplay: ''
      };
    }
    this.marketInterface = marketInterface;
  }

  registerOnChange(fn: () => void): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: () => void): void {
    this.onTouched = fn;
  }
}
