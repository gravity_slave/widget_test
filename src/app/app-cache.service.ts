import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';

@Injectable()
export class AppCacheService {
  private coins: Object[];
  private obsCoins: Subject<Object[]> = new Subject<Object[]>();
  constructor() {}

  getCoins(): Observable<Object[]> {
    return Observable.of(this.coins);
  }

  setCoins(coins: Object) {
    const coinKeys = Object.keys(coins).filter(key => key.startsWith('USDT'));
    this.coins = [];
    coinKeys.forEach(ck => this.coins.push(coins[ck]));
    this.obsCoins.next(this.coins);
  }
}
