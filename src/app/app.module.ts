import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AppService } from './app.service';
import { HttpClientModule } from '@angular/common/http';
import { CryptoHeaderComponent } from './components/crypto-header/crypto-header.component';
import { CryptoOptionsComponent } from './components/crypto-options/crypto-options.component';
import { FormsModule } from '@angular/forms';
import { AppCacheService } from './app-cache.service';

@NgModule({
  declarations: [AppComponent, CryptoHeaderComponent, CryptoOptionsComponent],
  imports: [BrowserModule, HttpClientModule, FormsModule],
  providers: [AppService, AppCacheService],
  bootstrap: [AppComponent]
})
export class AppModule {}
