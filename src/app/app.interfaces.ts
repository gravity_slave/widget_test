export interface MarketInterface {
  rowsToDisplay: number | string;
  rightColumn: string;
  filterWord?: string;
  quickSelect?: string;
}

type order = 'asc' | 'desc';
export type attribute = 'icon' | 'price' | 'volume' | 'coin' | 'change' | '';

export interface SortInterface {
  sortingAttr: attribute;
  sortingOrder: order;
}
